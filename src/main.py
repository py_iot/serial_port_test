
from typing import Tuple, Optional, Union, Dict

import PySimpleGUI as sg
from serial import Serial
from serial.tools import list_ports


BN_ON: str = "ON "
BN_OFF: str = "OFF"
BN_EXIT: str = "Закрыть"


def create_main_window(theme: str = "DarkBrown1") -> sg.Window:
    sg.theme(theme)

    serial_port_list: Tuple[str, ...] = tuple(str(i.device) for i in list_ports.grep(".*"))

    speed_port: Tuple[int, ...] = (110, 150, 300, 600, 1200, 2400, 4800, 9600, 19200, 38400, 56000, 57600, 115200)

    first_serial_port: Optional[str] = serial_port_list[0] if serial_port_list else None

    layout = [
        [
            sg.Combo(serial_port_list, default_value=first_serial_port, size=(20, 1), key="port"),
            sg.Combo(speed_port, default_value=9600, size=(20, 1), key="baudrate"),
        ],
        [
            sg.Output(size=(50, 10), key="output")
        ],
        [
            sg.Input(size=(50, 10), key="input")
        ],
        [
            sg.Button(BN_ON, key="let"),
            sg.Button("Send", key="send"),
            sg.Button(BN_EXIT, key="exit")
        ]
    ]

    window: sg.Window = sg.Window('Serial Port', layout)

    return window


def main():

    window: sg.Window = create_main_window()
    serial: Optional[Serial] = None
    config_serial: Optional[Dict[str, Union[str, int]]] = None

    while True:
        event, values = window.read(timeout=10)

        if event in (None, "exit", BN_EXIT):
            break

        if "let" == event:
            b_let: sg.Button = window["let"]
            on_off: bool = False

            if b_let.get_text() == BN_ON:
                b_let.update(text=BN_OFF)
                on_off = True
            else:
                b_let.update(text=BN_ON)
                on_off = False

            send: sg.Button = window["send"]
            b_input: sg.Input = window["input"]
            b_input.update(value="on" if on_off else "off")
            send.Click()

        if "send" == event:
            b_input: sg.Input = window["input"]
            value_input: str = b_input.get()
            serial.write(value_input.encode("utf-8"))
            print(f"-> {value_input}")

        value_copy = values.copy()
        value_copy.pop("input")

        if config_serial != value_copy:
            config_serial = value_copy

            if serial is None:
                serial = Serial(port=config_serial["port"], baudrate=config_serial["baudrate"])
            else:
                serial.close()
                serial = Serial(port=config_serial["port"], baudrate=config_serial["baudrate"])

        value: bytes = serial.read()

        if value:
            value_str: str = value.decode("utf-8")
            if value_str != "\r" and value_str != "\n":
                print(value_str)

    serial.close()
    window.close()


if __name__ == '__main__':
    main()

