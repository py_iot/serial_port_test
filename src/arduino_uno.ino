int led = 13;

String on = String("on");
String off = String("off");

int status = 0;

void setup() {
  Serial.begin(9600);
  pinMode(led, OUTPUT);
}

void loop() {

  if (Serial.available()) {
    String var = Serial.readString();

    if (var == on) {
      digitalWrite(led, HIGH);
      status = 1;
    } else if (var == off)
    {
      digitalWrite(led, LOW);
      status = 0;
    } else
    {
      status = 3;
    }
  }

  Serial.println(status);

  delay(500);
}
