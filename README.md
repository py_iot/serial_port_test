
Обезательно нужны права для чдение serial port
```bash
sudo usermod -a -G dialout $USER
```

установка зависимостей
```bash
    python3.7 -m venv venv
    source ./venv/bin/activate
    pip install -r ./requirements.txt

```

запуск
```bash
    cd src
    export PYTHONPATH=${PWD}
    python3.7 ./main.py
```
