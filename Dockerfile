# Use an official Python runtime as a parent image
FROM python:3.7-alpine

# Set the working directory to /app
WORKDIR /src

RUN apk upgrade --update
RUN apk add --no-cache gcc musl-dev linux-headers libffi-dev openssl-dev

# Copy the current directory contents into the container at /src/af

COPY ./src /src
COPY ./requirements.txt /src

# Install any needed packages specified in requirements.txt
RUN pip install -r requirements.txt

# Run app.py when the container launches
# Using sh -c to successfully read env variables
CMD ["sh", "-c", "python main.py"]
